// self代表子线程自身，即子线程的全局对象

import ExcelJS from 'exceljs'

// 以下几种方法都可以监听 message 事件
// self.addEventListener('message', () => {})
// this.addEventListener('message', () => {})
// addEventListener('message', () => {})
// self.onmessage = () => {}

self.addEventListener('message', e => {
  const {
    data: { columns, dataSource }
  } = e

  // 创建工作铺
  const workBook = new ExcelJS.Workbook()
  // 添加工作表
  const workSheet = workBook.addWorksheet('sheet1')

  // 添加数据
  const workBookColumns = columns.map(item => ({
    header: item.title,
    key: item.key,
    width: 32
  }))
  workSheet.columns = workBookColumns
  workSheet.addRows(dataSource)

  // 导出表格
  workBook.xlsx.writeBuffer().then(buffer => {
    const file = new Blob([buffer], {
      type: 'application/octet-stream'
    })
    // 将获取的数据通过postMessage发送到主线程
    self.postMessage({
      data: file,
      name: 'worker test'
    })
    // worker 内部关闭自身
    self.close()
  })
})
