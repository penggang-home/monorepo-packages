## 使用 worker 技术实现 Excel大文件导出
> 该案例是在掘金 [小羽同学] worker 系列文章思路下实现

参考：
- 阮一峰老师：[Web Worker 使用教程](https://www.ruanyifeng.com/blog/2018/07/web-worker.html)
- 掘金小羽同学：[项目没亮点？那就来学下web worker吧~](https://juejin.cn/post/7117774868187185188)
- 掘金小羽同学：[我不允许你们学会了worker却还没有应用场景](https://juejin.cn/post/7148239142806093838)

## 目的:性能优化
导出`3w`条数据情况下：
- 主线程导出,观察时间戳可以发现 **明显的卡顿**
- worker线程导出,观察时间戳可以发现 **无卡顿** 现象

## 截图
<img src="https://img-blog.csdnimg.cn/d87e578989214572b4c23b7a290a6492.gif"/>