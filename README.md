## 目的📇

这里放置一些精美、动手能力强、功能完善的小案例🤪

持续更新⚡

## 注意⚠️

这是一个 monorepo 风格的仓库
包管理器使用 [pnpm](https://www.pnpm.cn/)

## 运行🙃
首先 clone 该项目
```
git clone https://gitee.com/penggang-home/monorepo-packages.git
```

之后分两种情况:
- 纯 HTML 项目
   - 则直接进入该`package`,将`index.html`运行到浏览器即可查看。
- Vue、React 等其他项目
   1. 在根目录下 `pnpm install`
   2. 进入到你想要预览的`package`,执行`pnpm install`
   3. 然后 `npm run dev` 即可运行项目

## List👊

- [Apple-AirPods-Pro ](/packages/apple-airpods-pro/README.md) (模仿 AirPods-Pro 官网动画效果)
  <img src="https://img-blog.csdnimg.cn/4554c38bdd274835a393ae7b50d97441.gif"/>
- [markdown-vue3](/packages/markdown-vue3/README.md) (Vu3实现一个简单markdown编辑器)
  <img src="https://img-blog.csdnimg.cn/06422fae0e7f46ada5e3d939fa69a96c.png"/>
- [worker-export-excel](/packages/worker-export-excel/README.md) (worker实现excel大文件导出 性能优化)
  <img src="https://img-blog.csdnimg.cn/d87e578989214572b4c23b7a290a6492.gif"/>
- [file-upload](/packages/file-upload/README.md) (大文件上传 断点续传 文件秒传 含前后端)
